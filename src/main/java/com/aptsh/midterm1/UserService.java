/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aptsh.midterm1;

import java.util.ArrayList;

/**
 *
 * @author ASUS
 */
public class UserService {
    private static ArrayList<Product> userList = new ArrayList<>();
    private static Product currentUser = null;
    
    //Create
    public static boolean addProduct(Product product) {
        userList.add(product);
        return true;
    }
    //Delete
     public static boolean delProduct(Product product) {
        userList.remove(product);
        return true;
    }
     //Delete Product
      public static boolean delProduct(int index) {
        userList.remove(index);
        return true;
    }
      //read
       public static ArrayList<Product>getProducts(){
        return userList;
    }
       public static Product getUser(int index) {
        return userList.get(index);
    }
       //Update
       public static boolean updateUser(int index, Product product) {
        userList.set(index, product);
        return true;
    }

    public static boolean isLogin() {
        return currentUser != null;
    }

    public static Product getCurrentUser() {
        return currentUser;
    }

    static void load() {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
       
    
        
}
